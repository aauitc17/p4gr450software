package carcontroller

type Car struct {
	Speed int
}

func New() Car {
	return Car{}
}

func (c *Car) SetSpeedLimit(speedSign int) {
	switch speedSign {
	case 0:
		c.Speed = 60
	case 1:
		c.Speed = 80
	case 2:
		c.Speed = 110
	case 3:
		c.Speed = 130
	default:
		c.Speed = 0
	}
}
