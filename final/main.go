package main

import (
	"fmt"
	"time"

	"bitbucket.org/aauitc17/p4gr450nn/neuralNetwork"
	"bitbucket.org/aauitc17/p4gr450software/final/camera"
	"bitbucket.org/aauitc17/p4gr450software/final/carcontroller"
	"bitbucket.org/aauitc17/p4gr450software/final/cv"
	"bitbucket.org/aauitc17/p4gr450software/final/lifo"
)

func main() {
	// init our network
	size := []int{64 * 64, 40, 4}
	var net neuralNetwork.Network
	net.Init(size)
	net.LoadNetwork("../../p4gr450nn/network-based-on-graphs3.csv")

	// init camera LIFO queue
	cameraLifo := lifo.New(1)

	// init gocv LIFO queue
	cvLifo := lifo.New(1)

	// init our speed signs queue
	singsLifo := lifo.New(1)

	// init camera
	c, _ := camera.Init()

	// init our car
	car := carcontroller.New()

	// make a goroutine that graps frames
	// and appends to queue
	go func() {
		for {
			cameraLifo.Push(c.GrabFrame())
			time.Sleep(time.Millisecond * 10)
		}
	}()

	// process images using gocv
	go func() {
		for {
			img, err := cameraLifo.Pop()
			if err != nil {
				// sleep 1 millisecound before trying again
				time.Sleep(time.Millisecond)
				fmt.Println("GOCV WAITING!")
				continue
			}

			// sometimes we get a int back
			// but check
			switch img.(type) {
			case []byte:
			default:
				continue
			}

			// process the image, and add to our
			// cvLifo. Do not append, if the
			// image we got back has a length
			// of 0.
			imgProc := cv.ProcessImage(img.([]byte))
			if len(imgProc) == 0 {
				continue
			}

			cvLifo.Push(imgProc)
		}
	}()

	// process using our machine learning
	go func() {
		for {
			img, err := cvLifo.Pop()
			if err != nil {
				// sleep 1 millisecound before trying again
				time.Sleep(time.Millisecond)
				// fmt.Println("ML WAITING!")
				continue
			}

			sign := net.Guess(img.([]float64))
			singsLifo.Push(sign)
		}
	}()

	// we will just run our car, as the main prcess
	// since everything else is currently running in threads
	lastPrint := time.Now()
	startTime := time.Now()
	var signs int
	for {
		// try to pop a value, and set for our car!
		sign, err := singsLifo.Pop()
		if err != nil {
			// sleep 1 millisecound before trying again
			time.Sleep(time.Millisecond)
			// fmt.Println("SIGN WAITING!")
			continue
		}

		car.SetSpeedLimit(sign.(int))

		signs++

		if time.Since(lastPrint) > time.Second*2 {
			lastPrint = time.Now()
			fmt.Printf("Currently driving %d km/h, because that is the speed limit!\n", car.Speed)
			fmt.Printf("%f signs/sec\n", float64(signs)/float64(time.Since(startTime)/time.Second))
		}
	}
}
