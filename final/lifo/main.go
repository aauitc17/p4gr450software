package lifo

import (
	"errors"
	"sync"
)

var (
	ErrNoValues = errors.New("No values in LIFO queue")
)

type LIFO struct {
	sync.Mutex
	Values  []interface{}
	MaxSize int
}

func New(size int) LIFO {
	return LIFO{
		MaxSize: size,
	}
}

func (l *LIFO) Push(nValues interface{}) {
	l.Lock()
	defer l.Unlock()

	l.Values = append(l.Values, nValues)

	// remove Values if LIFO is too big
	for i := 0; i < len(l.Values)-l.MaxSize; i++ {
		l.Values = append(l.Values[:0], l.Values[0+1:]...)
	}
}

func (l *LIFO) Pop() (interface{}, error) {
	l.Lock()
	defer l.Unlock()

	// check if any Values to extract
	if len(l.Values) == 0 {
		return nil, ErrNoValues
	}

	v := l.Values[len(l.Values)-1]

	// remove from element
	l.Values = append(l.Values[:len(l.Values)-1], l.Values[len(l.Values)-1+1:]...)

	return v, nil
}
