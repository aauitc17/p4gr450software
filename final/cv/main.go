package cv

import (
	"image"
	"log"

	"gocv.io/x/gocv"
)

func normal(x int) float64 {
	return float64(x) / 256.0
}

// func tryImage(filename string, window *gocv.Window) []byte {
func ProcessImage(data []byte) []float64 {
	img, err := gocv.IMDecode(data, gocv.IMReadColor)
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	// inrange it (color ranges)
	lower := gocv.Scalar{10, 10, 59, 255}
	upper := gocv.Scalar{255, 255, 255, 255}
	inRange := gocv.NewMat()
	defer inRange.Close()
	gocv.InRangeWithScalar(img, lower, upper, &inRange)

	// find countours
	contours := gocv.FindContours(inRange, gocv.RetrievalTree, gocv.ChainApproxSimple)

	var biggest, area int
	biggestpoints := gocv.Mat{}
	defer biggestpoints.Close()
	tries := 0

	for _, cont := range contours {
		rect := gocv.BoundingRect(cont)
		area = (rect.Max.X - rect.Min.X) * (rect.Max.Y - rect.Min.Y)

		if (area < 500) || (area > 700*1000) {
			continue
		}

		if area >= biggest {
			biggestpoints.Close()
			tries++
			biggest = area
			biggestpoints = img.Region(rect)
		}

		// if it used more than 1 try, we assume
		// that something went wrong
		if tries > 1 {
			return []float64{}
		}
	}

	// make a new mat, for the cropped sign
	signMat := gocv.NewMat()
	defer signMat.Close()
	biggestpoints.CopyTo(&signMat)
	gocv.Resize(signMat, &signMat, image.Point{64, 64}, 0, 0, gocv.InterpolationCubic)
	gocv.CvtColor(signMat, &signMat, gocv.ColorRGBToGray)

	var imageMatrix []float64
	for i := 0; i < signMat.Rows(); i++ {
		for j := 0; j < signMat.Cols(); j++ {
			sFloat := normal(int(signMat.GetUCharAt(i, j)))
			imageMatrix = append(imageMatrix, sFloat)
		}
	}

	return imageMatrix
}
