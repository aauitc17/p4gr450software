package camera

import (
	"fmt"
	"io/ioutil"
	"log"
	"time"
)

type Camera interface {
	GrabFrame() []byte
}

type cam struct {
	StartTime time.Time
	FPS       int
	MaxFrames int
}

func Init() (cam, error) {
	return cam{
		StartTime: time.Now(),
		FPS:       24,
		MaxFrames: 1185 - 1,
	}, nil
}

func (c *cam) mapValue(input float64) int {
	var outputStart, outputEnd, inputStart, inputEnd float64
	outputStart = 1
	outputEnd = float64(c.MaxFrames)
	inputStart = 0
	inputEnd = float64((c.MaxFrames * 1000 / c.FPS * 1000) / 1000)

	return int(outputStart + ((outputEnd-outputStart)/(inputEnd-inputStart))*(input-inputStart))
}

func (c *cam) findFrame() int {
	elapsed := float64(time.Since(c.StartTime) / time.Millisecond)
	return (c.mapValue(elapsed) % c.MaxFrames) + 1
}

func (c *cam) GrabFrame() []byte {
	frameNumber := c.findFrame()
	imageName := fmt.Sprintf("camera/frames/%04d.jpg", frameNumber)
	// fmt.Println(imageName)

	data, err := ioutil.ReadFile(imageName)
	if err != nil {
		log.Fatal(err)
	}

	return data
}
