// Bench can be used to easily benchmark
// how long it takes to run a function.
package bench

import (
	"time"

	log "github.com/sirupsen/logrus"
)

// bench holds internal information.
type bench struct {
	start          time.Time // holds when we started this benchmark
	msg            string    // holds the messsage it should print, when we stop the benchmark
	TimespentMilli float64
	TimespentNano  int
	Verbose        bool
}

type Times struct {
	TimespentMilli float64
	TimespentNano  int
}

type Benchmark struct {
	Ourtime    Times
	Notourtime Times
	Nodes      int
}

// Init our bench class, which will give us
// a empty bench struct, which we can use
// to call other functions.
func Init() bench {
	// return our empty struct!
	return bench{
		start:   time.Time{},
		msg:     "EMPTY",
		Verbose: true,
	}
}

// Start is used to begin a new benchmark,
// which accepts a message, which it will
// print on Stop
func (b *bench) Start(msg string) {
	b.start = time.Now()
	b.msg = msg
}

// Stops our benchmark, prints out our message
// and how long it took since we started it.
func (b *bench) Stop() Times {
	if b.Verbose {
		// prints our the secounds and nanosecounds it took
		log.Infof("%s: %fs (%d nanosecounds)\n", b.msg, time.Since(b.start).Seconds(), time.Since(b.start))
	}

	b.TimespentMilli = float64(time.Since(b.start)) / float64(time.Millisecond)
	b.TimespentNano = int(time.Since(b.start))

	// reset it to empty
	b.start = time.Time{}
	b.msg = "EMPTY"

	return Times{
		TimespentMilli: b.TimespentMilli,
		TimespentNano:  b.TimespentNano,
	}
}
