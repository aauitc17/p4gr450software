package main

import (
	"fmt"

	"bitbucket.org/aauitc17/p4gr450nn/neuralNetwork"
	"bitbucket.org/aauitc17/p4gr450software/final/bench"
	"bitbucket.org/aauitc17/p4gr450software/final/camera"
	"bitbucket.org/aauitc17/p4gr450software/final/cv"
)

func main() {
	fmt.Println("what..!")
	// init our network
	size := []int{64 * 64, 5, 4}
	var net neuralNetwork.Network
	net.Init(size)
	// net.LoadNetwork("../../p4gr450nn/network-based-on-graphs3.csv")

	b := bench.Init()

	c, _ := camera.Init()

	// benchmark our grapper with append!
	b.Start("Time to process files using frame grabber and append")
	var frames [][]byte
	for i := 0; i < 500; i++ {
		frame := c.GrabFrame()
		frames = append(frames, frame)
	}
	b.Stop()

	// benchmark gocv
	b.Start("Time to process files using GOCV")
	var images [][]float64
	for i := 0; i < 500; i++ {
		frame := c.GrabFrame()
		imgProc := cv.ProcessImage(frame)
		if len(imgProc) == 0 {
			continue
		}
		images = append(images, imgProc)
	}
	b.Stop()

	b.Start("Time to proces images using ML")
	for _, img := range images {
		net.Guess(img)
	}
	b.Stop()
}
