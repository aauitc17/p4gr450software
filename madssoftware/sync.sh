#!/bin/bash
# sync.sh
DIR=raspcode/l
HOST=$1

if [ ! -d "$DIR" ]; then
    echo "Usage: ./sync <folder-to-sync>"
    echo "Directory \"$DIR\" does not exists"
    exit
fi


DIR=$(pwd)/$DIR

echo "Syncing folder"

echo $DIR

sync_folder() {
    rsync -avz --delete $DIR $HOST:madsraspcode
}

# initial sync
sync_folder

# keep pooling for data
while inotifywait -r -e modify,create,delete $DIR; do
    sync_folder
done
