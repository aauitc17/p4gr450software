package main

import (
	"fmt"
	"image"
	"image/color"
	"io/ioutil"
	"os"

	"gocv.io/x/gocv"
)

func main() {
	window := gocv.NewWindow("Billede viser")
	window.ResizeWindow(500, 500)

	dir := "../dataset/test/130/"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}
		fullPath := fmt.Sprintf("%s%s", dir, file.Name())
		tryImage(fullPath, window)
	}

}

func tryImage(filename string, window *gocv.Window) {
	img := gocv.IMRead(filename, gocv.IMReadGrayScale)

	thres := gocv.NewMat()
	gocv.AdaptiveThreshold(img, &thres, 125, gocv.AdaptiveThresholdMean, gocv.ThresholdBinary, 11, 35)

	// find countours
	contours := gocv.FindContours(thres, gocv.RetrievalTree, gocv.ChainApproxSimple)
	var biggest int
	var biggestCont []image.Point
	for _, cont := range contours {
		if len(cont) > biggest && gocv.BoundingRect(cont).Max.X != 1080 {
			biggest = len(cont)
			biggestCont = cont
			gocv.Rectangle(&thres, gocv.BoundingRect(biggestCont), color.RGBA{255, 255, 255, 255}, 2)
		}

	}
	gocv.Rectangle(&img, gocv.BoundingRect(biggestCont), color.RGBA{255, 255, 255, 255}, 2)
	// something := gocv.ApproxPolyDP(biggestCont, 1, true)
	// test := make([][]image.Point, 1)
	// test[0] = something
	// gocv.DrawContours(&img, test, -1, color.RGBA{255, 255, 255, 0}, 2)

	for {
		window.IMShow(img)
		key := window.WaitKey(1)
		_ = key
		if key >= 0 {
			if key == 'y' {
				keepImage(filename)
				// fmt.Println("KEEEEEP")
			} else if key == 'd' {
				destroyImage(filename)
				// fmt.Println("DELEEETE")
			} else if key == 'e' {
				os.Exit(0)
			} else {
				fmt.Println("Invalid key")
				continue
			}
			break
		}
	}
}

func keepImage(filename string) {
	fmt.Printf("Keeping image %s\n", filename)
}

func destroyImage(filename string) {
	fmt.Printf("DESTROYINGNGGNGNGNG image %s\n", filename)
	// err := os.Remove(filename)
	// if err != nil {
	// 	fmt.Println("Failed to delete stuff")
	// }
}
