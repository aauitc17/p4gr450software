// What it does:
//
// This example uses the Window class to open an image file, and then display
// the image in a Window class.
//
// How to run:
//
// 		go run ./cmd/showimage/main.go /home/ron/Pictures/mcp23017.jpg
//
// +build example

package main

import (
	"fmt"
	"gocv.io/x/gocv"
	//	"image/color"
)

func main() {
	fmt.Println("yay")
	// initial stuff
	window := gocv.NewWindow("Hello")
	img := gocv.IMRead("../testbilleder/80_skilte/00000_00029.ppm", gocv.IMReadColor)

	// ranges
	lb := gocv.Scalar{80, 70, 50, 0}
	up := gocv.Scalar{255, 255, 255, 255}
	imgRange := gocv.NewMat()
	gocv.InRangeWithScalar(img, lb, up, &imgRange)

	// apply mask
	imgMasked := gocv.NewMat()
	gocv.BitwiseAndWithMask(img, img, &imgMasked, imgRange)

	// grayscale original
	// var gray gocv.Mat
	gray := gocv.NewMat()
	gocv.CvtColor(imgMasked, &gray, gocv.ColorRGBToGray)

	// var circles gocv.Mat
	// circles := gocv.Mat{}
	circles := gocv.NewMat()
	gocv.HoughCircles(gray, &circles, gocv.HoughGradient, 2, 1)
	fmt.Println(circles)
	for i := 0; i < circles.Cols(); i++ {
		v := circles.GetVecfAt(0, i)
		fmt.Println(v)
	}

	for {
		window.IMShow(imgMasked)
		//window.IMShow(imgRange)
		// window.IMShow(gray)
		// window.IMShow(out)
		if window.WaitKey(1) >= 0 {
			break
		}
	}
}
