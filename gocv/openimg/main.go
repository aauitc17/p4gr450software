package main

import (
	"fmt"
	"gocv.io/x/gocv"
	"image"
	"image/color"
)

const (
	scalex  = 64
	scaley  = 48
	scalers = 0.9
)

func scaler(img gocv.Mat) gocv.Mat {
	point := image.Point{
		X: scalex,
		Y: scaley,
	}
	gocv.Resize(img, &img, point, 0, 0, 0)
	gocv.MedianBlur(img, &img, 1)
	return img
}

func circlejerker(img gocv.Mat, dp float64) gocv.Mat {
	var grayimg gocv.Mat
	grayimg = img
	gocv.CvtColor(grayimg, &grayimg, gocv.ColorRGBToGray)
	gocv.MedianBlur(grayimg, &grayimg, 5)
	cimg := gocv.NewMat()
	gocv.CvtColor(img, &cimg, gocv.ColorGrayToBGR)
	circles := gocv.NewMat()
	gocv.HoughCirclesWithParams(img, &circles, gocv.HoughGradient, dp, 0.1, 100, 100, 1, 500)
	blue := color.RGBA{0, 0, 255, 0}
	for i := 0; i < circles.Cols(); i++ {
		v := circles.GetVecfAt(0, i)
		fmt.Println(v)
		// if circles are found
		if len(v) > 2 {
			x := int(v[0])
			y := int(v[1])
			r := int(v[2])

			gocv.Circle(&cimg, image.Pt(x, y), r, blue, 2)
		}
	}
	return cimg
}

func colorjerker(img gocv.Mat) gocv.Mat {
	imgRange := gocv.NewMat()
	lb := gocv.Scalar{80, 70, 50, 0}
	up := gocv.Scalar{255, 255, 255, 255}
	// gocv.InRangeWithScalar(img, gocv.Scalar{0, 70, 50, 0}, gocv.Scalar{10, 255, 255, 0}, &img)
	gocv.InRangeWithScalar(img, lb, up, &imgRange)
	return imgRange
}

func countourfinder(img, mask gocv.Mat) (image.Point, int) {
	var biggest, area, lenght, broad, radius int
	biggestpoints := []image.Point{}
	red := color.RGBA{255, 0, 0, 0}
	points := gocv.FindContours(mask, gocv.RetrievalTree, gocv.ChainApproxSimple)
	for _, contour := range points {
		rect := gocv.BoundingRect(contour)
		fmt.Println(rect)
		//gocv.Rectangle(&img, rect, red, 1)
		area = (rect.Max.X - rect.Min.X) * (rect.Max.Y - rect.Min.Y)
		//area = gocv.ContourArea(contour)
		if (area < 50) || (area > 63*48) {
			continue
		}
		if area >= biggest {
			fmt.Printf("%d is bigger than %d\n", area, biggest)
			biggest = area
			biggestpoints = contour
			fmt.Println(biggestpoints)
		}
	}
	rect := gocv.BoundingRect(biggestpoints)
	lenght = (rect.Max.X - rect.Min.X)
	broad = (rect.Max.Y - rect.Min.Y)
	if lenght > broad {
		radius = lenght / 2
	} else {
		radius = broad / 2
	}
	centrum := image.Point{}
	centrum.X = (lenght / 2) + rect.Min.X
	centrum.Y = (broad / 2) + rect.Min.Y
	fmt.Println(area)
	fmt.Println("-----")
	fmt.Println(biggest)
	fmt.Println(biggestpoints)
	rect2 := gocv.BoundingRect(biggestpoints)
	gocv.Rectangle(&img, rect2, red, 1)
	_ = red
	//gocv.Circle(&img, centrum, radius, color.RGBA{255, 0, 0, 0}, -1)
	//fmt.Println(points)
	gocv.DrawContours(&img, points, -1, red, 1)
	return centrum, radius
}

func masker(img, mask gocv.Mat) gocv.Mat {
	test := gocv.NewMat()
	gocv.BitwiseAndWithMask(img, img, &test, mask)
	fmt.Print("test")
	return test
}

func main() {

	filename := "../testbilleder/80_skilte/00000_00026.ppm"
	filename = "../testbilleder/80.jpg"
	window := gocv.NewWindow("Billede viser")
	img := gocv.IMRead(filename, gocv.IMReadColor)

	if img.Empty() {
		fmt.Printf("Error reading image from: %v", filename)
		return
	}
	scaledimg := scaler(img)
	// originalimg := img
	mask := colorjerker(scaledimg)
	imgMasked := masker(scaledimg, mask)
	_ = imgMasked
	centrum, radius := countourfinder(img, mask)
	//mask = gocv.NewMatWithSize(64, 64, 1)
	fmt.Println(scaledimg.Size())
	mask = gocv.NewMatWithSize(48, 64, 0)
	gocv.Circle(&mask, centrum, radius, color.RGBA{255, 255, 255, 0}, -1)
	nytbillede := gocv.NewMat()
	_ = nytbillede
	gocv.BitwiseAndWithMask(scaledimg, scaledimg, &nytbillede, mask)
	// _ = mask
	// _ = window
	// _ = originalimg
	// _ = circleimg
	for {
		window.IMShow(nytbillede)
		if window.WaitKey(1) >= 0 {
			break
		}
	}
}
