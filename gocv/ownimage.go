package main

import (
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"sort"

	"gocv.io/x/gocv"
)

func normal(x int) float64 {
	return float64(x) / 256.0
}

type imageCont struct {
	area int
	cont []image.Point
}

func tryImage(filename string, skilttype string, window *gocv.Window) string {
	img := gocv.IMRead(filename, gocv.IMReadColor)
	imgGray := gocv.NewMat()
	gocv.CvtColor(img, &imgGray, gocv.ColorRGBToGray)
	lower := gocv.Scalar{10, 10, 59, 255}
	upper := gocv.Scalar{255, 255, 255, 255}

	inRange := gocv.NewMat()
	gocv.InRangeWithScalar(img, lower, upper, &inRange)
	// find countours
	contours := gocv.FindContours(inRange, gocv.RetrievalTree, gocv.ChainApproxSimple)
	areaConts := []imageCont{}
	for _, cont := range contours {
		rect := gocv.BoundingRect(cont)
		area := (rect.Max.X - rect.Min.X) * (rect.Max.Y - rect.Min.Y)

		areaConts = append(areaConts, imageCont{
			area: area,
			cont: cont,
		})
	}

	// sort slice
	sort.Slice(areaConts, func(i, j int) bool {
		return areaConts[i].area > areaConts[j].area
	})

	sImg := img.Region(gocv.BoundingRect(areaConts[1].cont))
	nImage := gocv.NewMat()
	sImg.CopyTo(&nImage)
	gocv.Resize(nImage, &nImage, image.Point{64, 64}, 0, 0, gocv.InterpolationCubic)
	gocv.CvtColor(nImage, &nImage, gocv.ColorRGBToGray)
	for {
		window.IMShow(nImage)
		gocv.IMWrite("130-sign-final.png", nImage)
		// window.IMShow(img)
		key := window.WaitKey(1)
		_ = key
		if key >= 0 {
			if key == 'y' {
				//	keepImage(filename)
				fmt.Println("KEEEEEP")
			} else if key == 'd' {
				//	destroyImage(filename)
				fmt.Println("DELEEETE")
			} else if key == 'e' {
				os.Exit(0)
			} else {
				fmt.Println("Invalid key")
				continue
			}
			break
		}
	}
	return ""
}

func main() {
	window := gocv.NewWindow("Billede viser")
	window.ResizeWindow(500, 500)

	dir := "../dataset/projektbilleder/130/"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println(err)
		return
	}

	zeroimages := ""
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		fullPath := fmt.Sprintf("%s%s", dir, file.Name())
		fmt.Println(fullPath)
		zeroimages += tryImage(fullPath, "0", window)
	}

	//ioutil.WriteFile("60images.csv", []byte(zeroimages), os.ModePerm)

}
