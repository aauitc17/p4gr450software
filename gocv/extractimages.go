package main

import (
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"gocv.io/x/gocv"

	"bitbucket.org/aauitc17/p4gr450algworkshop2/bench"
)

func normal(x int) float64 {
	return float64(x) / 256.0
}

// func tryImage(filename string, window *gocv.Window) []byte {
func tryImage(filename string) []byte {
	b := bench.Init()
	b.Start("Reading image and pre process")
	img := gocv.IMRead(filename, gocv.IMReadColor)
	defer img.Close()

	// grayscale shit

	// inrange it (color ranges)
	lower := gocv.Scalar{10, 10, 59, 255}
	upper := gocv.Scalar{255, 255, 255, 255}
	inRange := gocv.NewMat()
	defer inRange.Close()
	gocv.InRangeWithScalar(img, lower, upper, &inRange)
	b.Stop()

	// find countours
	contours := gocv.FindContours(inRange, gocv.RetrievalTree, gocv.ChainApproxSimple)

	var biggest, area int
	biggestpoints := gocv.Mat{}
	defer biggestpoints.Close()
	tries := 0

	for _, cont := range contours {
		rect := gocv.BoundingRect(cont)
		area = (rect.Max.X - rect.Min.X) * (rect.Max.Y - rect.Min.Y)

		if (area < 500) || (area > 700*1000) {
			continue
		}

		if area >= biggest {
			biggestpoints.Close()
			tries++
			biggest = area
			biggestpoints = img.Region(rect)
		}
	}

	// if it used more than 1 try, we assume
	// that something went wrong
	if tries > 1 {
		return []byte{}
	}

	// make a new mat, for the cropped sign
	signMat := gocv.NewMat()
	defer signMat.Close()
	biggestpoints.CopyTo(&signMat)
	gocv.Resize(signMat, &signMat, image.Point{64, 64}, 0, 0, gocv.InterpolationCubic)
	gocv.CvtColor(signMat, &signMat, gocv.ColorRGBToGray)

	var imageByte []byte
	for i := 0; i < signMat.Rows(); i++ {
		for j := 0; j < signMat.Cols(); j++ {
			sFloat := strconv.FormatFloat(normal(int(signMat.GetUCharAt(i, j))), 'f', -1, 64)
			imageByte = append(imageByte, sFloat+","...)
			// imageByte += fmt.Sprintf("%s,", sFloat)
		}
	}

	imageByte = imageByte[:len(imageByte)-1]
	if strings.Contains(filename, "60") {
		imageByte = append(imageByte, ",0\n"...)
	} else if strings.Contains(filename, "80") {
		imageByte = append(imageByte, ",1\n"...)
	} else if strings.Contains(filename, "110") {
		imageByte = append(imageByte, ",2\n"...)
	} else if strings.Contains(filename, "130") {
		imageByte = append(imageByte, ",3\n"...)
	}

	return imageByte
}

func main() {
	// window := gocv.NewWindow("Billede viser")
	// window.ResizeWindow(500, 500)

	// dir := "../dataset/test/130/"
	// dir := "../dataset/projektbilleder/130/"
	// dir := "../dataset/newdataset/signs/"
	dir := "../dataset/projektbilleder/signs/"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println(err)
		return
	}

	var finalCsv []byte
	// sign := "130_"
	// current := 2000
	index := -1
	for _, file := range files {
		if file.IsDir() {
			continue
		}

		index++
		fmt.Println(index)

		fullPath := fmt.Sprintf("%s%s", dir, file.Name())
		csv := tryImage(fullPath)
		finalCsv = append(finalCsv, csv...)
		break

	}
	_ = finalCsv

	// ioutil.WriteFile(fmt.Sprintf("new-%s-%d.csv", sign, current), finalCsv, os.ModePerm)
	// ioutil.WriteFile("new-testing-rpi.csv", finalCsv, os.ModePerm)
	ioutil.WriteFile("new-testing.csv", finalCsv, os.ModePerm)

	// fmt.Println(string(finalCsv))

}
